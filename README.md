# OmniFilterPlugin

This is a Sylius e-commerce plugin that enables attribute and price filters.
Attribute filter lists the possible values of certain attributes found in the 
products and a user can select only the products with desired attributes.
Search filter is a simple range filter that provides the possibility to filter
out the products that have certain prices. An example of how such filters
look is provided bellow:

![filters](/docs/img/filters.png "Filters")

### How it works?

`tagIndex` field is appended to product table which holds all attribute-value tags. 
This field is used to filter product using fulltext search.

Use `bin/console omni:filter:index` for initial product indexing.

## Installation

### Step 1: add bundle to Kernel

```php
    public function registerBundles()
    {
        $bundles = [
            // ...
            new \Omni\Sylius\FilterPlugin\OmniSyliusFilterPlugin(),
            new \Omni\Sylius\CorePlugin\OmniSyliusCorePlugin(),
        ];
        
        // ...
    }
```

###  Step 2: add configuration

Add configuration:
```
# app/config/config.yml
 
imports:
    - { resource: "@OmniSyliusFilterPlugin/Resources/config/config.yml" }
    
omni_sylius_filter:
  attribute_choice_formation_strategy: intuitive
```

`attribute_choice_formation_strategy` parameter is optional. This
parameter determines how the attribute filter will form the available choices.
Read more about it below in the dedicated section.

###  Step 3: update product mapping

Use `bin/console doctrine:schema:update -f` or DoctrineMigrations.

###  Step 4: extend ProductAttribute back office template:

```twig
# app/Resources/SyliusAdminBundle/views/ProductAttribute/_form.html.twig  
 
{% include '@OmniSyliusFilterPlugin/ProductAttribute/_form.html.twig' %}
```

## Usage

After the installation the price filter will appear in the product list 
by default but the attribute filter will be nowhere to be seen. In order 
to enable this filter please go to the admin panel of the application and
go to the product attributes list. Choose which product attributes should be 
filterable and edit them. Attribute form has an additional field:

![filterable attribute](/docs/img/filterable_attribute.png "Filterable attribute")

After you make your wanted attributes filterable you must run the index 
command again:

```bash
$ bin/console omni:filter:index
```

Now if you go to your products you will see that they are in fact filterable
by attribute.

### Attribute choice formation strategy

Attribute filter forms choices that you can choose from when filtering attributes.
when you render a product index page (aka the category page) there is a choice provider
service that retrieves available choices from the back end and provides them to the 
form type. The choices array looks like this:

```
  choices = [
    'simple_attribute_1' => [
       'option a' => 3,  # here 3 is the amount of products containing the option
       'option b' => 2,
       # ...
    ],
    'selectable_attribute_1' => [ # select type attributes have different structure
       'option a' => [
          '_label' => 'Option A',
          '_amount' => 3,
       ],
       # ...
    ],
    'attribute_2' => [
      # ...
    ],
    # ...
  ]
```

There are however, three strategies to choose from for the formation of this array.
The desired strategy should be provided to the configuration of the plugin (see 
configuration section).

Available strategies are (choose wisely):
- `static` - the choices are being formed from all the products that are retrieved by 
the restrictions of other filters. That is if you have a color and material
attributes, the choices and available amounts of possible products displayed will 
not change when you select a different color or material.
- `restrictive` - if you select a certain value of an attribute the choices with
zero possible products will disappear, also, the amounts of possible products will
change accordingly. Please have in mind, that if you have products with different 
colors and every product can only have one color, using this strategy, if you
will select one of the colors all other color choices will disappear until you will
undo your choice of color.
- `intuitive` - this is the best strategy for product choice display, but it comes
at a cost. And the cost is performance... With this strategy, the amounts of available
products with every choice will change when you will start selecting choices. The available
product counts in a single attribute choices will stay logical even when a given attribute
is active. It will also display zero choices and it is up to you whether you want to
show them or not. Have in mind though, that using this strategy means that 
the amount of queries made to the database for choice formation is equal to the number
of selected attributes (not choices) + 1. So it is not recommended if you have 
a large shop with >100 000 products.
  