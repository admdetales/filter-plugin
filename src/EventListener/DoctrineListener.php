<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\EventListener;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Omni\Sylius\FilterPlugin\Indexer\TagManager;
use Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface;
use Sylius\Component\Attribute\Model\Attribute;
use Sylius\Component\Product\Model\ProductAttributeValueInterface;

class DoctrineListener
{
    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var string
     */
    private $attributeClass;

    /**
     * @var Attribute[]
     */
    private $filterableAttributes;

    /**
     * @param TagManager $tagManager
     * @param string     $attributeClass
     */
    public function __construct(TagManager $tagManager, $attributeClass)
    {
        $this->tagManager = $tagManager;
        $this->attributeClass = $attributeClass;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->reindex($args);
    }

    /**
     * Happens after updating a product.
     *
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->reindex($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        // TODO: update product tagIndex on ProductAttributeValue removal
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function reindex(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof ProductAttributeValueInterface
            && $entity->getSubject() instanceof SearchTagAwareInterface
        ) {
            /** @var SearchTagAwareInterface $product */
            $product = $entity->getSubject();
            $tags = $this->tagManager->extractTags($product, $this->getFilterableAttributes($args->getEntityManager()));
            $tags = $this->tagManager->buildTagIndex($tags);
            $product->setTagIndex($tags);
        }
    }

    /**
     * @param EntityManager $em
     *
     * @return Attribute[]
     */
    private function getFilterableAttributes(EntityManager $em)
    {
        if (null === $this->filterableAttributes) {
            /** @var Attribute[] $filterableAttributes */
            $this->filterableAttributes = $em->getRepository($this->attributeClass)->getFilterableAttributes();
        }

        return $this->filterableAttributes;
    }
}
