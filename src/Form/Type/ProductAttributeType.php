<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Form\Type;

use Omni\Sylius\FilterPlugin\Doctrine\ORM\ProductAttributeRepository;
use Omni\Sylius\FilterPlugin\Grid\Controller\ChoicesProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductAttributeType extends AbstractType
{
    /**
     * @var ProductAttributeRepository
     */
    private $productAttributeRepository;

    /**
     * @var ChoicesProvider
     */
    private $choicesProvider;

    /**
     * @var bool
     */
    private $showProductsAmount;

    /**
     * @param ProductAttributeRepository $productAttributeRepository
     * @param ChoicesProvider $choicesProvider
     * @param bool $showProductsAmount
     */
    public function __construct(
        ProductAttributeRepository $productAttributeRepository,
        ChoicesProvider $choicesProvider,
        bool $showProductsAmount
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->choicesProvider = $choicesProvider;
        $this->showProductsAmount = $showProductsAmount;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = $this->choicesProvider->getChoices();
        $labelTemplate = $this->showProductsAmount ? '%s (%d)'  : '%s';

        if (null === $attributes = $this->productAttributeRepository->getFilterableAttributes()) {
            return;
        }

        foreach ($attributes as $attribute) {
            if (false === isset($choices[$attribute->getCode()])) {
                continue;
            }
            switch ($attribute->getType()) {
                case 'text':
                    $builder->add($attribute->getCode(), ChoiceType::class, [
                        // Turns the initial array structure of ['foo' => 3, 'bar' => 4, 'acme' => 1]
                        // into a structure that is needed for the choices:
                        // ['foo (3)' => 'foo', 'bar (4)' => 'bar', 'acme (1) => acme]
                        'choices' => array_combine(
                            array_map(
                                function ($option, $amount) use ($labelTemplate) {
                                    return sprintf($labelTemplate, ucfirst($option), $amount);
                                },
                                array_keys($choices[$attribute->getCode()]),
                                array_values($choices[$attribute->getCode()])
                            ),
                            array_keys($choices[$attribute->getCode()])
                        ),
                        'label' => $attribute->getTranslation()->getName(),
                        'expanded' => true,
                        'multiple' => true,
                    ]);
                    break;
                case 'select':
                    $builder->add($attribute->getCode(), ChoiceType::class, [
                        // Acts similarly to the one above, but handles selectable option data and its complexities
                        'choices' => array_combine(
                            array_map(
                                function ($value) use ($labelTemplate) {
                                    return sprintf($labelTemplate, ucfirst($value['_label']), $value['_amount']);
                                },
                                array_values($choices[$attribute->getCode()])
                            ),
                            array_keys($choices[$attribute->getCode()])
                        ),
                        'label' => $attribute->getTranslation()->getName(),
                        'expanded' => true,
                        'multiple' => true,
                    ]);
                    break;
                case 'integer':
                    break;
                case 'checkbox':
                    break;
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefault('clear_attribute_button', false)
            ->setDefault('clear_all_button', false)
            ->setAllowedTypes('clear_attribute_button', ['bool'])
            ->setAllowedTypes('clear_all_button', ['bool'])
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars['clear_attribute_button'] = $options['clear_attribute_button'];
        $view->vars['clear_all_button'] = $options['clear_all_button'];
    }
}
