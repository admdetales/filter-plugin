<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\FilterPlugin\Form\Type;

use Omni\Sylius\FilterPlugin\Doctrine\ORM\ProductAttributeRepository;
use Omni\Sylius\FilterPlugin\Grid\Controller\ChoicesProvider;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceFilterType extends AbstractType
{
    /**
     * @var ChoicesProvider
     */
    private $choicesProvider;

    /**
     * @param ChoicesProvider  $choicesProvider
     */
    public function __construct(ChoicesProvider $choicesProvider)
    {
        $this->choicesProvider = $choicesProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $lowerPrice = round($this->choicesProvider->getLowerPrice() / 100, 2);
        $upperPrice = round($this->choicesProvider->getUpperPrice() / 100, 2);

        $builder
            ->add('lower', HiddenType::class, [
                'empty_data' => $lowerPrice,
                'attr' => [
                    'data-lower' => $lowerPrice,
                ]
            ])
            ->add('upper', HiddenType::class, [
                'empty_data' => $upperPrice,
                'attr' => [
                    'data-upper' => $upperPrice,
                ]
            ])
        ;
    }
}
